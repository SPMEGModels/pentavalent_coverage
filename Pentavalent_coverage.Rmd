---
title: "Pentavalent vaccine  coverage"
output:
  word_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.width = 10, fig.height = 6)
```


# R library used


```{r lib, message=FALSE, warning=FALSE, paged.print=FALSE}
library(tidyverse)

library(readxl)
library(lme4)
library(cAIC4)
library(knitr)

options(dplyr.print_max = 500)
cb1 <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
cb2 <-c("#E41A1C", "#377EB8", "#4DAF4A", "#984EA3","#FF7F00", "#FFFF33", "#A65628", "#F781BF", "#999999")
cb <- c(cb1,cb2) 

```


# Read data from Excel file (WUENIC file : July 2019)

Coverage data used in this analysis can be found at the following address <http://www.who.int/entity/immunization/monitoring_surveillance/data/coverage_estimates_series.xls?ua=1>.


```{r pressure, echo=TRUE, message=FALSE, warning=FALSE}
read_excel_allsheets <- function(filename) {
  sheets <- readxl::excel_sheets(filename)
  x <- lapply(sheets, function(X) readxl::read_excel(filename, sheet = X))
  x <- lapply(x, as.data.frame)
  names(x) <- sheets
  x
}
alldata = read_excel_allsheets("Coverage_data.xls")
df <- alldata$Sheet1
(n_countries <- n_distinct(df$Cname)) 
df %>% group_by (IncomeGroup) %>% summarise(n=n_distinct(Cname)/n_countries) 
df %>% group_by (Region) %>% summarise(n=n_distinct(Cname)/n_countries) 

```


# Data filtering

## Countries using wP pentavalent

```{r}
df2 = df %>% filter(Countrieswithwholecell==1)
(n_countries <- n_distinct(df2$Cname)) 
df2 %>% group_by (IncomeGroup) %>% summarise(n=n_distinct(Cname)/n_countries) 
df2 %>% group_by (Region) %>% summarise(n=n_distinct(Cname)/n_countries) 

```
## Countries used in the model

```{r}
df3 = subset(df2, Pentaintro!=0 & MCV1 > 0 & BCG > 0) 
(n_countries <- n_distinct(df3$Cname)) 
df3 %>% group_by (IncomeGroup) %>% summarise(n=n_distinct(Cname)/n_countries) 
df3 %>% group_by (Region) %>% summarise(n=n_distinct(Cname)/n_countries) 

```


## Vaccine introduction period


```{r echo = FALSE}

Intro_penta = df3%>%
  group_by(Cname)%>%
  summarise(DTP=1,HEP= min(HepB_before_Penta),HIB= min(Hib_before_Penta)) 

dat <- gather(Intro_penta, variable, value, -Cname)

xf <- factor(dat$value, levels = c(0, 1),labels = c("After Pentavelent", "Before Pentalent"))
xv <- factor(dat$variable, levels = c('HIB','HEP','DTP'),labels = c("HiB", "Hepatitis B", "DTP"))

ggplot(dat, aes(x=xv, fill=xf)) +
  theme_classic() +
  geom_bar() + 
  labs(x = "Vaccine",
       y = "Number of countries", 
       title ="Vaccine introdution according to Pentavalent use",
       fill = "Time of introduction")  

```


# DTP coverage


## Data used for DTP analysis


```{r}
df_dtp = subset(df3, DTP3 > 0) 
(n_countries <- n_distinct(df_dtp$Cname)) 
df_dtp %>% group_by (IncomeGroup) %>% summarise(n=n_distinct(Cname)/n_countries) 
df_dtp %>% group_by (Region) %>% summarise(n=n_distinct(Cname)/n_countries) 

```


## Mixed effect model with BCG as proxy variable


```{r}
dtp_b <- lmer (DTP3 ~ (1|Cname) + BCG + Post_penta,data = df_dtp, REML = F)
summary(dtp_b)
cAIC(dtp_b)
confint(dtp_b)
p_mb <- as.numeric(predict(dtp_b))
rmse <- mean((df_dtp$DTP3 - p_mb)^2)^0.5
cat("\n rmse=",rmse)

```


## Mixed effect model with MCV1 as proxy variable


```{r}

dtp_m <- lmer (DTP3 ~ (1|Cname) + MCV1 + Post_penta,data = df_dtp, REML = F)
summary(dtp_m)
cAIC(dtp_m)
confint(dtp_m)
p_mb <- as.numeric(predict(dtp_m))
rmse <- mean((df_dtp$DTP3 - p_mb)^2)^0.5
cat("\n rmse=",rmse)

```


## Mixed effect model with BCG + MCV1 as proxy variables


```{r}

dtp_mb <- lmer (DTP3 ~ (1|Cname) + MCV1 + BCG + Post_penta,data = df_dtp, REML = F)
summary(dtp_mb)
cAIC(dtp_mb)
confint(dtp_mb)
p_mb <- as.numeric(predict(dtp_mb))
rmse <- mean((df_dtp$DTP3 - p_mb)^2)^0.5
cat("\n rmse=",rmse)

df_test<-df_dtp
df_test$Post_penta = 0
p_no <- as.numeric(predict(dtp_mb,df_test))
rmse_no <- mean((df_dtp$DTP3 - p_no)^2)^0.5
cat("\n rmse without pentavalent effect=",rmse_no)

```


## Observed versus estimated - annual average


```{r message=FALSE, warning=FALSE, echo=FALSE}

res <- data.frame(dtp=df_dtp$DTP3 , yr= df_dtp$Year, cy= df_dtp$Cname, pred = p_mb, pred_no = p_no)
tmp = res %>%
  group_by(yr) %>%
  summarise_if(is.numeric, mean)

gg <- ggplot(tmp, aes(x = yr, y = dtp))
gg <- gg + theme_classic() 
gg <- gg + labs(x = "Year",  y = "DTP3 coverage (%)") 
gg <- gg +   geom_line(aes(y = dtp,colour="Observed"),size=1 )
gg <- gg +   geom_line(aes(y = pred,colour="Estimated with pentavalent effect"),size=1)
gg <- gg +   geom_line(aes(y = pred_no,colour="Estimated without pentavalent effect"),size=1)
gg <- gg +   scale_y_continuous(limits = c(0,100))  
gg <- gg +   theme(legend.title=element_blank(),legend.position="top", legend.text=element_text(size=15)) 
gg <- gg +   theme(axis.title.x = element_text(size=15))
gg <- gg +   theme(axis.title.y = element_text(size=15))
gg <- gg + scale_colour_manual(values=cb)
gg

ggsave("dtp3.png", gg, width = 10, height = 6)

```
## Observed versus estimated - scatter plot


```{r message=FALSE, warning=FALSE, echo=FALSE}

length(res$pred[res$pred >100])
length(res$pred[res$pred <0])
length(res$pred)


gg <- ggplot(res)
gg <- gg +   geom_point (aes(x = dtp, y = pred))
gg <- gg + geom_abline(intercept=0, slope=1)
gg <- gg +   theme(legend.title=element_blank(),legend.position="top") 
gg <- gg + theme_bw()+ scale_colour_manual(values=cb) + scale_y_continuous(breaks=seq(-10,110,10))
gg

```
## Mixed effect model with logit transformation 


```{r}

require(gtools)
df_dtp$LDTP3 <- logit(df_dtp$DTP3/100)
df_dtp$LBCG <- logit(df_dtp$BCG/100)
df_dtp$LMCV1 <- logit(df_dtp$MCV1/100)


dtp_logit <- lmer (LDTP3 ~ (1|Cname) + LBCG + LMCV1 + Post_penta,data = df_dtp, REML = F)
summary(dtp_logit)
cAIC(dtp_logit)
confint(dtp_logit)
p_mb <- as.numeric(100*inv.logit(predict(dtp_logit)))
rmse <- mean((df_dtp$DTP3 - p_mb)^2)^0.5
cat("\n rmse=",rmse)

df_test<-df_dtp
df_test$Post_penta = 0
p_no <- as.numeric(100*inv.logit(predict(dtp_logit,df_test)))
rmse_no <- mean((df_dtp$DTP3 - p_no)^2)^0.5
cat("\n rmse without pentavalent effect=",rmse_no)

```
## Observed versus estimated - annual average (logit)


```{r message=FALSE, warning=FALSE, echo=FALSE}

res <- data.frame(dtp=df_dtp$DTP3 , yr= df_dtp$Year, cy= df_dtp$Cname, pred = p_mb, pred_no = p_no)
tmp = res %>%
  group_by(yr) %>%
  summarise_if(is.numeric, mean)

gg <- ggplot(tmp, aes(x = yr, y = dtp))
gg <- gg + theme_classic() 
gg <- gg + labs(x = "Year",  y = "DTP3 coverage (%)", size = 2) 
gg <- gg +   geom_line(aes(y = dtp,colour="Observed"),size=1 )
gg <- gg +   geom_line(aes(y = pred,colour="Estimated with pentavalent effect"),size=1)
gg <- gg +   geom_line(aes(y = pred_no,colour="Estimated without pentavalent effect"),size=1)
gg <- gg +   scale_y_continuous(limits = c(0,100))  
gg <- gg +   theme(legend.title=element_blank(),legend.position="top", legend.text=element_text(size=15)) 
gg <- gg +   theme(axis.title.x = element_text(size=15))
gg <- gg +   theme(axis.title.y = element_text(size=15))
gg <- gg + scale_colour_manual(values=cb)
gg

```
## Observed versus estimated - scatter plot (logit)


```{r message=FALSE, warning=FALSE, echo=FALSE}


gg <- ggplot(res)
gg <- gg +   geom_point (aes(x = dtp, y = pred))
gg <- gg + geom_abline(intercept=0, slope=1)
gg <- gg +   theme(legend.title=element_blank(),legend.position="top") 
gg <- gg + theme_bw()+ scale_colour_manual(values=cb)  + scale_y_continuous(breaks=seq(-10,110,10))
gg

```

# HepB coverage


## Data used for the HepB analysis


```{r}
df_hep = subset(df3, HEPB3 > 0 & HepB_before_Penta > 0) 
(n_countries <- n_distinct(df_hep$Cname)) 
df_hep %>% group_by (IncomeGroup) %>% summarise(n=n_distinct(Cname)/n_countries) 
df_hep %>% group_by (Region) %>% summarise(n=n_distinct(Cname)/n_countries) 

```


## Mixed effect model with BCG and MCV1 as proxy variable


```{r}
hep <- lmer (HEPB3 ~ (1|Cname) + MCV1 + BCG + Post_penta,data = df_hep, REML = FALSE)
summary(hep)
cAIC(hep)
confint(hep)

h_mb <- as.numeric(predict(hep))
rmse <- mean((df_hep$HEPB3 - h_mb)^2)^0.5
cat("\n rmse=",rmse)

df_test<-df_hep
df_test$Post_penta = 0
h_no <- as.numeric(predict(hep,df_test))
rmse_no <- mean((df_hep$HEPB3 - h_no)^2)^0.5
cat("\n rmse without pentavalent effect=",rmse_no)



```


## Observed versus estimated


```{r message=FALSE, warning=FALSE, echo= FALSE}
res <- data.frame(df_hep, pred = h_mb, pred_no = h_no)
tmp = res %>%
  group_by(Year) %>%
  summarise_if(is.numeric, mean, na.rm = TRUE)

gg <- ggplot(tmp, aes(x = Year))
gg <- gg + theme_classic() 
gg <- gg + labs(x = "Year",  y = "HEPB3 coverage (%)") 
gg <- gg +   geom_line(aes(y = HEPB3,colour="Observed"),size=1 )
gg <- gg +   geom_line(aes(y = pred,colour="Estimated with pentavalent effect"),size=1)
gg <- gg +   geom_line(aes(y = pred_no,colour="Estimated without pentavalent effect"),size=1)
gg <- gg +   scale_y_continuous(limits = c(0,100))  
gg <- gg +   theme(legend.title=element_blank(),legend.position="top", legend.text=element_text(size=15)) 
gg <- gg +   theme(axis.title.x = element_text(size=15))
gg <- gg +   theme(axis.title.y = element_text(size=15))
gg <- gg + scale_colour_manual(values=cb)
gg

res %>%
  select(Year, HEPB3) %>%
  group_by(Year) %>%
  summarise_if(is.numeric, min, na.rm = TRUE)


```
## Observed versus estimated - scatter plot


```{r message=FALSE, warning=FALSE, echo=FALSE}

length(res$pred[res$pred >100])
length(res$pred[res$pred <0])
length(res$pred)

gg <- ggplot(res)
gg <- gg +   geom_point (aes(x = HEPB3, y = pred))
gg <- gg + geom_abline(intercept=0, slope=1)
gg <- gg +   theme(legend.title=element_blank(),legend.position="top") 
gg <- gg + theme_bw()+ scale_colour_manual(values=cb)  + scale_y_continuous(breaks=seq(-10,110,10))
gg

```

# Hib coverage


## Data used for the HepB analysis


```{r}
df_hib = subset(df3, HIB3 > 0 & Hib_before_Penta > 0) 
(n_countries <- n_distinct(df_hib$Cname)) 
df_hib %>% group_by (IncomeGroup) %>% summarise(n=n_distinct(Cname)/n_countries) 
df_hib %>% group_by (Region) %>% summarise(n=n_distinct(Cname)/n_countries) 

```


## Mixed effect model with BCG and MCV1 as proxy variable


```{r}

hib <- lmer (HIB3 ~ (1|Cname) + MCV1 + BCG + Post_penta,data = df_hib, REML = FALSE)
summary(hib)
cAIC(hib)
confint(hib)

hi_mb <- as.numeric(predict(hib))
rmse <- mean((df_hib$HIB3 - hi_mb)^2)^0.5
cat("\n rmse=",rmse)

df_test<-df_hib
df_test$Post_penta = 0
hi_no <- as.numeric(predict(hib,df_test))
rmse_no <- mean((df_hib$HIB3 - hi_no)^2)^0.5
cat("\n rmse without pentavalent effect=",rmse_no)


```


## Observed versus estimated


```{r echo = FALSE}

res <- data.frame(df_hib, pred = hi_mb, pred_no = hi_no)
tmp = res %>%
  group_by(Year) %>%
  summarise_if(is.numeric, median, na.rm = TRUE)

gg <- ggplot(tmp, aes(x = Year))
gg <- gg + theme_classic() 
gg <- gg + labs(x = "Year",  y = "HIB3 coverage (%)") 
gg <- gg +   geom_line(aes(y = HIB3,colour="Observed"),size=1 )
gg <- gg +   geom_line(aes(y = pred,colour="Estimated with pentavalent effect"),size=1)
gg <- gg +   geom_line(aes(y = pred_no,colour="Estimated without pentavalent effect"),size=1)
gg <- gg +   scale_y_continuous(limits = c(0,100))  
gg <- gg +   theme(legend.title=element_blank(),legend.position="top", legend.text=element_text(size=15)) 
gg <- gg +   theme(axis.title.x = element_text(size=15))
gg <- gg +   theme(axis.title.y = element_text(size=15))
gg <- gg + scale_colour_manual(values=cb)
gg


```
## Observed versus estimated - scatter plot


```{r message=FALSE, warning=FALSE, echo=FALSE}

length(res$pred[res$pred >100])
length(res$pred[res$pred <0])
length(res$pred)

gg <- ggplot(res)
gg <- gg +   geom_point (aes(x = HIB3, y = pred))
gg <- gg + geom_abline(intercept=0, slope=1)
gg <- gg +   theme(legend.title=element_blank(),legend.position="top") 
gg <- gg + theme_bw()+ scale_colour_manual(values=cb)  + scale_y_continuous(breaks=seq(-10,110,10))
gg

```
