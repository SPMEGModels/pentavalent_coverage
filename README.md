# Pentavalent impact on pediatric vaccination coverage

The pentavalent_coverage project contains the code enabling to produce the results
presented in the article entitled "Code corresponding to the article "How did the adoption of wP-pentavalent affect the global pediatric vaccine coverage rate?"

The project is organized as follows:
- code
  + model_function.R: the SEIR transmission model itself, with utility functions,
                   to load inputs or plot graphs for example, and general
                   parameters
  + compare_incid.R: the code to generate graph comparing different scenarios.
  + graph_hosp.R: the code to generate hospitalisations graph for a range of vaccine efficacy.
- input : this folder contains all the input files used for generating the different scenarios.
- result: The folder to store result files corresponding to each scenario
- figure: The folder to store graph allowing comparing scenario results
              

## Installation & execution

The code was developed and tested with R version 4.0.4 (2021-02-15) through
R Studio. 

### Dependencies
4 libraries need to be present on your R system:
- tidyverse
- readxl
- cAIC4
- knitr


### Execution

1. Open the RStudio project
2. Launch the R notebook "Pentavalent_coverage"

## Disclaimers

- The code is provided to ensure transparency and reproductability of graphs and
  figures exposed in the article. No specific support for someone willing to reuse this
  model in another context will be provided.
- We have developed and checked thoroughly this code. Nevertheless, an error is
  possible. We would be grateful if you consider contacting us, should you find
  any errors.

